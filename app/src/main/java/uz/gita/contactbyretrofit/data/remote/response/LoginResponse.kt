package uz.gita.contactbyretrofit.data.remote.response

data class LoginResponse(
    val token:String,
    val phone:String
)