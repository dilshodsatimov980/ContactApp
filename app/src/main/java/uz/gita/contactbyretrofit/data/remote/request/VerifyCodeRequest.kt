package uz.gita.contactbyretrofit.data.remote.request

data class VerifyCodeRequest (
    val phone:String,
    val code:String
)