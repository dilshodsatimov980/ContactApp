package uz.gita.contactbyretrofit.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object MyClient {

    val retrofit = Retrofit.Builder()
        .baseUrl("https://c7da-195-158-16-140.ngrok-free.app/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}