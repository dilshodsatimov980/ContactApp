package uz.gita.contactbyretrofit.data.remote.request

data class LoginRequest (
    val phone:String,
    val password:String
)