package uz.gita.contactbyretrofit.data.local.shared

import android.content.Context
import android.content.SharedPreferences

class MyShared private constructor(context: Context){

    private var pref:SharedPreferences
    init {
        pref = context.getSharedPreferences("Contact",Context.MODE_PRIVATE)
    }
    companion object{
        private lateinit var instance:MyShared
        fun init(context: Context){
            if (!(::instance.isInitialized)) instance = MyShared(context)
        }
        fun getInstance():MyShared = instance
    }

    fun setTokenToShared(token:String){
        pref.edit().putString("Token",token).apply()
    }
    fun getTokenFromShared():String = pref.getString("Token","@")!!

}