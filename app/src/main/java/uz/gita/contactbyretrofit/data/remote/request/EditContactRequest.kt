package uz.gita.contactbyretrofit.data.remote.request

import android.os.Parcel
import android.os.Parcelable

data class EditContactRequest(
    val id:Int,
    val firstName:String,
    val lastName:String,
    val phone:String
)