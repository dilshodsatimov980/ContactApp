package uz.gita.contactbyretrofit.data.remote.response
data class ErrorResponse(
    val message: String
)