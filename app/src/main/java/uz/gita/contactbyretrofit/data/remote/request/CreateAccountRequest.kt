package uz.gita.contactbyretrofit.data.remote.request

data class CreateAccountRequest (
    val firstName:String,
    val lastName:String,
    val phone:String,
    val password:String
)