package uz.gita.contactbyretrofit.data.remote.response

class CreateAccountResponse(
    val token:String,
    val phone:String
)