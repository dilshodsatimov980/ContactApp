package uz.gita.contactbyretrofit.data.remote.api

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query
import uz.gita.contactbyretrofit.data.remote.request.CreateContactRequest
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.data.remote.response.CreateAccountResponse
import uz.gita.contactbyretrofit.data.remote.response.ErrorResponse
import uz.gita.contactbyretrofit.data.remote.response.LoginResponse

interface MyApi {

    @POST("api/v1/register")
    fun createAccount(@Body data:CreateAccountRequest):Call<ErrorResponse>

    @POST("api/v1/register/verify")
    fun verifySmsCode(@Body data:VerifyCodeRequest):Call<CreateAccountResponse>

    @GET("api/v1/contact")
    fun getAllContact(@Header("token") token:String):Call<List<ContactResponse>>

    @POST("api/v1/contact")
    fun addContact(@Header("token") token:String, @Body data: CreateContactRequest) :Call<Unit>

    @DELETE("api/v1/contact")
    fun deleteContact(@Header("token") token:String, @Query("id") id:Int):Call<Unit>

    @POST("api/v1/login")
    fun enterByLogin(@Body login:LoginRequest):Call<LoginResponse>

    @PUT("api/v1/contact")
    fun editContact(@Header("token") token:String,@Body data:EditContactRequest):Call<ContactResponse>
}