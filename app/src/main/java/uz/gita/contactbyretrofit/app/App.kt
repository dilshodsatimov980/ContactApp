package uz.gita.contactbyretrofit.app

import android.app.Application
import uz.gita.contactbyretrofit.data.local.shared.MyShared

class App:Application() {
    override fun onCreate() {
        super.onCreate()
        MyShared.init(this)
    }
}