package uz.gita.contactbyretrofit.presentation.screen.viewModel

import androidx.lifecycle.LiveData
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest

interface LoginViewModel {

    val createLiveData:LiveData<Unit>
    val signInLiveData:LiveData<Unit>
    fun onClickCreate()
    fun onClickSignIn(login:LoginRequest)
}