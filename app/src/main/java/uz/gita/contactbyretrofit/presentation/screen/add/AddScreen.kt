package uz.gita.contactbyretrofit.presentation.screen.add

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.request.CreateContactRequest
import uz.gita.contactbyretrofit.databinding.ScreenAddBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.ContactViewModel
import uz.gita.contactbyretrofit.util.myAddTextChangeListener

class AddScreen:Fragment(R.layout.screen_add) {
    private var _binding:ScreenAddBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModels<ContactViewModel>()
    private var prepareFirstName = false
    private var prepareLastName = false
    private var preparePhone = false




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = ScreenAddBinding.bind(view)




        binding.apply {
            back.setOnClickListener {
                findNavController().popBackStack()
            }
            saveBtn.setOnClickListener {
                onClickSave()
            }
            editFirstName.myAddTextChangeListener {
                prepareFirstName = it.length>3
                check()
            }
            editLastName.myAddTextChangeListener {
                prepareLastName = it.length>3
                check()
            }
            editNumber.myAddTextChangeListener {
                preparePhone = it.startsWith("+998") && it.length==13
                check()
            }
        }
    }

    private fun  onClickSave(){
       val firstName = binding.editFirstName.text.toString()
       val lastName = binding.editLastName.text.toString()
       val phone = binding.editNumber.text.toString()
        if (firstName.isNotEmpty()&&lastName.isNotEmpty()&&phone.isNotEmpty()){
            val contact = CreateContactRequest(firstName, lastName, phone)
            viewModel.addContact(contact)
            findNavController().popBackStack()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
    private fun check(){
        binding.saveBtn.isEnabled = prepareFirstName && prepareLastName && preparePhone
    }

}