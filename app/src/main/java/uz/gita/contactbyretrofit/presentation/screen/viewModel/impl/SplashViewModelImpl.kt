package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.gita.contactbyretrofit.domain.AppRepository
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.presentation.screen.viewModel.SplashViewModel

class SplashViewModelImpl() : SplashViewModel,ViewModel() {
    override val progressLiveData = MutableLiveData<Boolean>()
    override val message = MutableLiveData<String>()
    private val repository:AppRepository = AppRepositoryImpl.getInstance()

    override fun openLoginOrContact() {
        progressLiveData.value = true
        if (repository.getTokenFromShared()=="@"){
            message.value = "Login"
            progressLiveData.value = false
        }else{
            message.value = "Contact"
            progressLiveData.value = false
        }
    }
}