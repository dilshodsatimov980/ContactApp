package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest
import uz.gita.contactbyretrofit.domain.AppRepository
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.presentation.screen.viewModel.LoginViewModel

class LoginViewModelImpl: LoginViewModel,ViewModel() {
    override val createLiveData = MutableLiveData<Unit>()
    override val signInLiveData =  MediatorLiveData<Unit>()
    private val repository:AppRepository = AppRepositoryImpl.getInstance()


    override fun onClickCreate() {
       createLiveData.value = Unit
    }

    override fun onClickSignIn(login:LoginRequest) {
      signInLiveData.addSource(repository.enterByLogin(login)){
          it.onSuccess {
              signInLiveData.value = Unit
              repository.setTokenToShared(it.token)
          }
              .onFailure {  }
      }
    }
}