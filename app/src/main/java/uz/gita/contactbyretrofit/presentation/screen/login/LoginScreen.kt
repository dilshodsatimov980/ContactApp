package uz.gita.contactbyretrofit.presentation.screen.login

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest
import uz.gita.contactbyretrofit.databinding.ScreenLoginBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.LoginViewModel
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.LoginViewModelImpl
import uz.gita.contactbyretrofit.util.myAddTextChangeListener

class LoginScreen:Fragment(R.layout.screen_login) {
    private var _binding:ScreenLoginBinding? = null
    private val binding get() =  _binding!!
    private val viewModel: LoginViewModel by viewModels<LoginViewModelImpl>()
    private var preparePhone = false
    private var preparePassword = false

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = ScreenLoginBinding.bind(view)

        binding.apply {
            create.setOnClickListener {
                viewModel.onClickCreate()
            }
            signIn.setOnClickListener {
                val phone = editPhone.text.toString()
                val password = editPassword.text.toString()
                val login = LoginRequest(phone, password)
                viewModel.onClickSignIn(login)
            }
            editPhone.myAddTextChangeListener {
                preparePhone = it.startsWith("+9989") && it.length ==13
                check()
            }
            editPassword.myAddTextChangeListener {
                preparePassword = it.length>3
                check()
            }
        }
        viewModel.createLiveData.observe(this,createObserver)
        viewModel.signInLiveData.observe(this,signObserver)


    }

    private fun check() {
       binding.signIn.isEnabled = preparePhone && preparePassword
    }

    private val createObserver = Observer<Unit>{
        findNavController().navigate(LoginScreenDirections.actionLoginScreenToCreateScreen())
    }
    private val signObserver = Observer<Unit> {
        findNavController().navigate(LoginScreenDirections.actionLoginScreenToContactScreen())
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}