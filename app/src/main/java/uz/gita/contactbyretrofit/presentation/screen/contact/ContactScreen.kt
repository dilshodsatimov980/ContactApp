package uz.gita.contactbyretrofit.presentation.screen.contact

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.databinding.ScreenContactBinding
import uz.gita.contactbyretrofit.presentation.adapter.ContactAdapter
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.ContactViewModel

class ContactScreen:Fragment(R.layout.screen_contact) {
    private var _binding:ScreenContactBinding? = null
    private val binding get() = _binding!!
    private val adapter by lazy { ContactAdapter() }
    private val viewModel by viewModels<ContactViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.openLoginLiveData.observe(this,openLoginObserver)

    }
    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = ScreenContactBinding.bind(view)
        viewModel.loadContact()
        binding.apply {
            rv.adapter = adapter
            rv.layoutManager = LinearLayoutManager(requireContext())
            addBtn.setOnClickListener {
                findNavController().navigate(ContactScreenDirections.actionContactScreenToAddScreen())
            }
            logOut.setOnClickListener {
                viewModel.onClickLogOut()
            }
            refreshLayout.setOnRefreshListener {
                viewModel.loadContact()
            }
        }

        viewModel.progressLiveData.observe(viewLifecycleOwner,progressObserver)
        viewModel.errorLiveData.observe(viewLifecycleOwner,errorObserver)
        viewModel.contactLiveData.observe(viewLifecycleOwner,contactObserver)
        adapter.setOnLongClickListener {
            showDialog(it)
        }
    }

    private val openLoginObserver = Observer<Unit>{
        findNavController().navigate(ContactScreenDirections.actionContactScreenToLoginScreen())
    }

    private val progressObserver = Observer<Boolean>{
        binding.refreshLayout.isRefreshing = it
    }
    private val contactObserver = Observer<List<ContactResponse>>{
        adapter.submitList(it)
    }
    private val errorObserver = Observer<String>{
        Toast.makeText(requireContext(),it,Toast.LENGTH_SHORT).show()
    }
    private fun  showDialog(data:ContactResponse){
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(R.layout.bottom_sheet)

        dialog.findViewById<ImageView>(R.id.dialog_delete)?.setOnClickListener {
            viewModel.deleteContact(data.id)
            dialog.dismiss()
        }

        dialog.findViewById<ImageView>(R.id.dialog_edit)?.setOnClickListener {
            val directions = ContactScreenDirections.actionContactScreenToEditScreen(
                data.id,
                data.firstName,
                data.lastName,
                data.phone
            )
            findNavController().navigate(directions)
            dialog.dismiss()
        }

        dialog.show()

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}