package uz.gita.contactbyretrofit.presentation.screen.edit

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest
import uz.gita.contactbyretrofit.databinding.ScreenEditBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.EditViewModel
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.EditViewModelImpl
import uz.gita.contactbyretrofit.util.myAddTextChangeListener

class EditScreen:Fragment(R.layout.screen_edit) {
    private val binding by viewBinding(ScreenEditBinding::bind)
    private val navArgs:EditScreenArgs by navArgs()
    private val viewModel:EditViewModel by viewModels<EditViewModelImpl>()
    private var prepareFirstName = true
    private var prepareLastName = true
    private var preparePhone = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    private val backObserver = Observer<Unit>{
        findNavController().popBackStack()
    }
    private val changeObserver = Observer<Unit>{
        findNavController().popBackStack()
    }

    private fun init(){
        val id = navArgs.id
        navArgs.apply {
            binding.editFirstName.setText(navArgs.firstName)
            binding.editLastName.setText(navArgs.lastName)
            binding.editNumber.setText(navArgs.phone)
        }
        binding.back.setOnClickListener {
            viewModel.onClickBackBtn()
        }

        binding.changeBtn.setOnClickListener {
            val firstName = binding.editFirstName.text.toString()
            val lastName = binding.editLastName.text.toString()
            val phone = binding.editNumber.text.toString()
            val editedContact = EditContactRequest(id, firstName, lastName, phone)
            viewModel.onClickChangeBtn(editedContact)
        }
        viewModel.backLiveData.observe(viewLifecycleOwner,backObserver)
        viewModel.changeLiveData.observe(viewLifecycleOwner,changeObserver)

        binding.apply {
            check()
            editFirstName.myAddTextChangeListener {
                prepareFirstName = it.length>3
                check()
            }
            editLastName.myAddTextChangeListener {
                prepareLastName = it.length>3
                check()
            }
            editNumber.myAddTextChangeListener {
                preparePhone = it.startsWith("+9989") && it.length == 13
                check()
            }
        }
    }

    private fun check() {
       binding.changeBtn.isEnabled = prepareFirstName && prepareLastName && preparePhone
    }
}