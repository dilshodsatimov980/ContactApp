package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest
import uz.gita.contactbyretrofit.domain.AppRepository
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.presentation.screen.viewModel.CreateViewModel

class CreateViewModelImpl : CreateViewModel, ViewModel() {
    override val backLiveData = MutableLiveData<Unit>()
    override val messageLiveData = MediatorLiveData<String>()
    override val openSmsDialog = MutableLiveData<Unit>()
    private val repository: AppRepository = AppRepositoryImpl.getInstance()


    override fun onClickBack() {
        backLiveData.value = Unit
    }

    override fun onClickSignUp(account: CreateAccountRequest) {
        messageLiveData.addSource(repository.createAccount(account)) {
            it.onSuccess {
                if (it.isSuccessful){
                    openSmsDialog.value = Unit
                }else messageLiveData.value = it.message()
                Log.d("TTT", "onFailure $it")
            }
                .onFailure {
                    Log.d("TTT", "onFailure ${it.message}")
                    messageLiveData.value = it.message
                }
        }
    }
}