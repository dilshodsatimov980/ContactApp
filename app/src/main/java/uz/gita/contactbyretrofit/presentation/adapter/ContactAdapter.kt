package uz.gita.contactbyretrofit.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.databinding.ItemContactBinding

class ContactAdapter:ListAdapter<ContactResponse,ContactAdapter.ContactViewHolder>(ContactDiffUtil) {
    private var onLongClickListener:((ContactResponse)->Unit)?= null


    object ContactDiffUtil:DiffUtil.ItemCallback<ContactResponse>(){
        override fun areItemsTheSame(oldItem: ContactResponse, newItem: ContactResponse): Boolean {
            return  oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ContactResponse,
            newItem: ContactResponse,
        ): Boolean {
            return oldItem.firstName == newItem.firstName
                    && oldItem.lastName == newItem.firstName
                    && oldItem.phone == newItem.phone
        }

    }

    inner class ContactViewHolder(private val binding:ItemContactBinding):ViewHolder(binding.root){


        @SuppressLint("SetTextI18n")
        fun bind() {
            binding.root.setOnLongClickListener {
                onLongClickListener?.invoke(getItem(absoluteAdapterPosition))

                return@setOnLongClickListener true
            }
            getItem(absoluteAdapterPosition).apply {
                binding.itemName.text = "$lastName  $firstName"
                binding.itemNumber.text = phone
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder  =
        ContactViewHolder(ItemContactBinding.inflate(LayoutInflater.from(parent.context),parent,false))


    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind()
    }

    fun setOnLongClickListener(block:(ContactResponse)->Unit){
        onLongClickListener = block
    }
}