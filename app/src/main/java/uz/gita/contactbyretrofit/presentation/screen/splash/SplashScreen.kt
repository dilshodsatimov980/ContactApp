package uz.gita.contactbyretrofit.presentation.screen.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.databinding.ScreenSplashBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.SplashViewModel
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.SplashViewModelImpl

class SplashScreen:Fragment(R.layout.screen_splash) {
    private var _binding:ScreenSplashBinding? =null
    private val binding get() = _binding!!
    private val viewModel: SplashViewModel by viewModels<SplashViewModelImpl> ()

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = ScreenSplashBinding.bind(view)

        viewModel.progressLiveData.observe(this,processObserver)
        viewModel.message.observe(this,messageObserver)
        viewModel.openLoginOrContact()
    }

    private val processObserver = Observer<Boolean>{
        if (it) binding.progressBar.show()
        else binding.progressBar.hide()
    }
    private val messageObserver = Observer<String>{
        when(it){
            "Contact" ->
                findNavController().navigate(SplashScreenDirections.actionSplashScreenToContactScreen())

            "Login" ->
                findNavController().navigate(SplashScreenDirections.actionSplashScreenToLoginScreen())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}