package uz.gita.contactbyretrofit.presentation.screen.viewModel

import androidx.lifecycle.LiveData
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest

interface EditViewModel {
    val backLiveData:LiveData<Unit>
    val changeLiveData :LiveData<Unit>

    fun onClickBackBtn()
    fun onClickChangeBtn(data:EditContactRequest)
}