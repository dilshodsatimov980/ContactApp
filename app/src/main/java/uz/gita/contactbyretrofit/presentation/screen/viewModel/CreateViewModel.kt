package uz.gita.contactbyretrofit.presentation.screen.viewModel

import androidx.lifecycle.LiveData
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest

interface CreateViewModel {

    val backLiveData:LiveData<Unit>
    val messageLiveData:LiveData<String>
    val openSmsDialog:LiveData<Unit>


    fun onClickBack()
    fun onClickSignUp(account:CreateAccountRequest)

}