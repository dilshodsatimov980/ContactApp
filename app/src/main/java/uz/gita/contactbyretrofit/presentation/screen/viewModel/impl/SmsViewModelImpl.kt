package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest
import uz.gita.contactbyretrofit.domain.AppRepository
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.presentation.screen.viewModel.SmsViewModel

class SmsViewModelImpl:SmsViewModel,ViewModel() {
    override val messageLiveData = MediatorLiveData<String>()
    override val openContactScreenLiveData = MutableLiveData<Unit>()
    private val repository:AppRepository = AppRepositoryImpl.getInstance()

    override fun onClickSubmit(data:VerifyCodeRequest) {
        messageLiveData.addSource(repository.verifySmsCode(data)){
            it.onSuccess {
                if (it.isSuccessful){
                   repository.setTokenToShared(it.body()!!.token)

                    //Log.d("TTT", "onClickSubmit: ${repository.getTokenFromShared()} ")
                    openContactScreenLiveData.value = Unit
                }
                else messageLiveData.value = it.message()
            }
                .onFailure {
                    messageLiveData.value = it.message
                }
        }
    }
}