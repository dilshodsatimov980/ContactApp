package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest
import uz.gita.contactbyretrofit.domain.AppRepository
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.presentation.screen.viewModel.EditViewModel

class EditViewModelImpl:EditViewModel, ViewModel() {
    override val backLiveData = MutableLiveData<Unit>()
    override val changeLiveData= MutableLiveData<Unit>()
    private val repository:AppRepository = AppRepositoryImpl.getInstance()

    override fun onClickBackBtn() {
        backLiveData.value = Unit
    }

    override fun onClickChangeBtn(data: EditContactRequest) {
        repository.editContact(
            data,
            successBlock = {
                changeLiveData.value = Unit
            },
            errorBlock = {

            }
        )
    }
}