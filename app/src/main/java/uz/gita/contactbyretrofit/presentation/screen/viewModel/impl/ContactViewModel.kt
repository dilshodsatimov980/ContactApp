package uz.gita.contactbyretrofit.presentation.screen.viewModel.impl

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.gita.contactbyretrofit.data.remote.MyClient
import uz.gita.contactbyretrofit.data.remote.api.MyApi
import uz.gita.contactbyretrofit.data.remote.request.CreateContactRequest
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.data.remote.response.ErrorResponse
import uz.gita.contactbyretrofit.domain.AppRepositoryImpl
import uz.gita.contactbyretrofit.util.MyEventBus

class ContactViewModel:ViewModel() {
    private val api = MyClient.retrofit.create(MyApi::class.java)
    private val repository = AppRepositoryImpl.getInstance()
    val progressLiveData = MutableLiveData<Boolean>()
    val errorLiveData = MediatorLiveData<String>()
    val contactLiveData = MutableLiveData<List<ContactResponse>>()
    val openLoginLiveData = MutableLiveData<Unit>()
    init {
        MyEventBus.reloadContact={
            loadContact()
        }
    }

    fun loadContact(){
        progressLiveData.value = true
        errorLiveData.addSource(repository.getAllContacts()){
            it.onSuccess {
                progressLiveData.value = false
                contactLiveData.value= it
            }
                .onFailure {
                    progressLiveData.value = false
                    errorLiveData.value = it.message
                }
        }


    }
    fun addContact(contact:CreateContactRequest){
        progressLiveData.value=true
        errorLiveData.addSource(repository.addContact(contact)){
            it.onSuccess {
                loadContact()
                MyEventBus.reloadContact?.invoke()
            }
                .onFailure {
                    progressLiveData.value = false
                    errorLiveData.value = it.message
                }
        }
    }
    fun deleteContact(id:Int){
        progressLiveData.value = true
        errorLiveData.addSource(repository.deleteContact(id)){
            it.onSuccess {
                loadContact()
            }
                .onFailure { errorLiveData.value = it.message }
        }
    }
    fun onClickLogOut(){
        repository.setTokenToShared("@")
        openLoginLiveData.value = Unit
    }



}