package uz.gita.contactbyretrofit.presentation.screen.sms

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest
import uz.gita.contactbyretrofit.databinding.ScreenSmsBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.SmsViewModel
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.SmsViewModelImpl

class SmsScreen:Fragment(R.layout.screen_sms) {

    private val binding by viewBinding(ScreenSmsBinding::bind)
    private val viewModel:SmsViewModel  by viewModels<SmsViewModelImpl>()
    private val navArgs:SmsScreenArgs by navArgs()

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            submit.setOnClickListener {
                val code = editSmsCode.text.toString()
                val phone = navArgs.phone
                val  data = VerifyCodeRequest(phone, code)
                viewModel.onClickSubmit(data)
            }
        }
        viewModel.messageLiveData.observe(this,messageObserver)
        viewModel.openContactScreenLiveData.observe(this,openContactObserver)
    }
    private val messageObserver = Observer<String>{
        Toast.makeText(requireContext(),it,Toast.LENGTH_SHORT).show()
    }
    private val openContactObserver = Observer<Unit>{
        findNavController().navigate(SmsScreenDirections.actionSmsScreenToContactScreen())
    }

}