package uz.gita.contactbyretrofit.presentation.screen.viewModel

import androidx.lifecycle.LiveData

interface SplashViewModel {
    val progressLiveData : LiveData<Boolean>
    val message:LiveData<String>
    fun openLoginOrContact()
}