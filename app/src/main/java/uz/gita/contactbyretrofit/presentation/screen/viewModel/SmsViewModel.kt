package uz.gita.contactbyretrofit.presentation.screen.viewModel

import androidx.lifecycle.LiveData
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest

interface SmsViewModel {
    val messageLiveData:LiveData<String>
    val openContactScreenLiveData:LiveData<Unit>

    fun onClickSubmit(data:VerifyCodeRequest)
}