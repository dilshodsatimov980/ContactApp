package uz.gita.contactbyretrofit.presentation.screen.create
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import uz.gita.contactbyretrofit.R
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest
import uz.gita.contactbyretrofit.databinding.ScreenCreateBinding
import uz.gita.contactbyretrofit.presentation.screen.viewModel.CreateViewModel
import uz.gita.contactbyretrofit.presentation.screen.viewModel.impl.CreateViewModelImpl
import uz.gita.contactbyretrofit.util.myAddTextChangeListener

class CreateScreen:Fragment(R.layout.screen_create) {
    private var _binding:ScreenCreateBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CreateViewModel by viewModels<CreateViewModelImpl>()
    private lateinit var phone:String
    private var prepareFistName = false
    private var prepareLastName = false
    private var preparePhone = false
    private var preparePassword = false


    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = ScreenCreateBinding.bind(view)
        viewModel.openSmsDialog.observe(this,openSmsObserver)
        viewModel.backLiveData.observe(this,backObserver)
        viewModel.messageLiveData.observe(this,messageObserver)

        binding.apply {
            back.setOnClickListener {
                viewModel.onClickBack()
            }

            signUp.setOnClickListener {
                val firstName = editFirstName.text.toString()
                val lastName = editLastName.text.toString()
                 phone = editPhone.text.toString()
                val password = editPassword.text.toString()
                val account = CreateAccountRequest(firstName, lastName, phone, password)
                viewModel.onClickSignUp(account)
            }

            editFirstName.myAddTextChangeListener {
                prepareFistName = it.length>3
                check()
            }
            editLastName.myAddTextChangeListener {
                prepareLastName = it.length>3
                check()
            }
            editPhone.myAddTextChangeListener {
                preparePhone = it.startsWith("+998") && it.length==13
                check()
            }
            editPassword.myAddTextChangeListener {
                preparePassword = it.length>3
                check()
            }
        }

    }

    private fun check() {
        binding.signUp.isEnabled = prepareFistName && prepareLastName && preparePhone && preparePassword
    }

    private val messageObserver = Observer<String>{
        object :CountDownTimer(500,10000){
            override fun onTick(millisUntilFinished: Long) {
                binding.errorText.text = it
            }
            override fun onFinish() {
                binding.errorText.text = ""
            }
        }.start()
    }
    private val openSmsObserver = Observer<Unit>{
        findNavController().navigate(CreateScreenDirections.actionCreateScreenToSmsScreen(phone))
    }
    private val backObserver = Observer<Unit>{
        findNavController().popBackStack()
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
