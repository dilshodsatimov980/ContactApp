package uz.gita.contactbyretrofit.util

import androidx.core.widget.addTextChangedListener
import com.vicmikhailau.maskededittext.MaskedEditText


    fun MaskedEditText.myAddTextChangeListener(block:(String)->Unit){
        this.addTextChangedListener {editable->
            editable?.let {
                block.invoke(it.toString())
            }
        }
    }

    fun MaskedEditText.text() = this.text.toString()
