package uz.gita.contactbyretrofit.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.gita.contactbyretrofit.data.local.shared.MyShared
import uz.gita.contactbyretrofit.data.remote.MyClient
import uz.gita.contactbyretrofit.data.remote.api.MyApi
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest
import uz.gita.contactbyretrofit.data.remote.request.CreateContactRequest
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.data.remote.response.CreateAccountResponse
import uz.gita.contactbyretrofit.data.remote.response.ErrorResponse
import uz.gita.contactbyretrofit.data.remote.response.LoginResponse

class AppRepositoryImpl private constructor():AppRepository {

    companion object{
        private lateinit var instance:AppRepository
        fun getInstance():AppRepository{
            if (!(::instance.isInitialized)) instance = AppRepositoryImpl()
            return instance
        }
    }

    private val api = MyClient.retrofit.create(MyApi::class.java)
    private val myShared = MyShared.getInstance()
    override fun createAccount(account: CreateAccountRequest): LiveData<Result<Response<ErrorResponse>>>{
        val resultLiveData = MutableLiveData<Result<Response<ErrorResponse>>>()

        api.createAccount(account).enqueue(object :Callback<ErrorResponse>{
            override fun onResponse(call: Call<ErrorResponse>, response: Response<ErrorResponse>) {
               resultLiveData.value = Result.success(response)
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
               resultLiveData.value = Result.failure(t)
            }
        })
        return resultLiveData
    }

    override fun verifySmsCode(data: VerifyCodeRequest): LiveData<Result<Response<CreateAccountResponse>>> {
        val resultLiveData = MutableLiveData<Result<Response<CreateAccountResponse>>>()
        api.verifySmsCode(data).enqueue(object:Callback<CreateAccountResponse>{
            override fun onResponse(
                call: Call<CreateAccountResponse>,
                response: Response<CreateAccountResponse>,
            ) {
                resultLiveData.value = Result.success(response)
            }
            override fun onFailure(call: Call<CreateAccountResponse>, t: Throwable) {
               resultLiveData.value = Result.failure(t)
            }
        })
        return  resultLiveData
    }


    override fun getAllContacts(): LiveData<Result<List<ContactResponse>>> {
        val resulLiveData = MutableLiveData<Result<List<ContactResponse>>>()
        val token = myShared.getTokenFromShared()
        api.getAllContact(token).enqueue(object: Callback<List<ContactResponse>>{
            override fun onResponse(
                call: Call<List<ContactResponse>>,
                response: Response<List<ContactResponse>>,
            ) {
                if (response.isSuccessful && response.body()!=null){
                    resulLiveData.value = Result.success(response.body()!!)
                }else resulLiveData.value = Result.success(emptyList())
            }

            override fun onFailure(call: Call<List<ContactResponse>>, t: Throwable) {
               resulLiveData.value = Result.failure(t)
            }

        })
        return resulLiveData
    }

    override fun addContact(data: CreateContactRequest): LiveData<Result<Unit>> {
        val resultLiveData = MutableLiveData<Result<Unit>>()
        api.addContact(myShared.getTokenFromShared(),data).enqueue(object :Callback<Unit>{
            override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                if (response.isSuccessful && response.body()!=null) {
                    resultLiveData.value = Result.success(Unit)
                }
            }
            override fun onFailure(call: Call<Unit>, t: Throwable) {
               resultLiveData.value = Result.failure(t)
            }
        })
        return resultLiveData
    }

    override fun deleteContact(id: Int): LiveData<Result<Unit>> {
        val resultLiveData = MutableLiveData<Result<Unit>>()
        api.deleteContact(myShared.getTokenFromShared(),id).enqueue(
            object :Callback<Unit>
            {
                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    if (response.isSuccessful && response.body()!=null)
                        resultLiveData.value = Result.success(Unit)
                }

                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    resultLiveData.value = Result.failure(t)
                }
            })
        return  resultLiveData
    }

    override fun enterByLogin(login: LoginRequest): LiveData<Result<LoginResponse>> {
        val resultLiveData = MutableLiveData<Result<LoginResponse>>()
        api.enterByLogin(login).enqueue(object:Callback<LoginResponse>{
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful && response.body()!=null)
                    resultLiveData.value = Result.success(response.body()!!)
            }
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
               resultLiveData.value = Result.failure(t)
            }
        })
        return  resultLiveData
    }

    override fun setTokenToShared(token: String) = myShared.setTokenToShared(token)

    override fun getTokenFromShared(): String  = myShared.getTokenFromShared()
    override fun editContact(
        editedContact: EditContactRequest,
        successBlock: () -> Unit,
        errorBlock: (String) -> Unit,
    ) {
       api.editContact(myShared.getTokenFromShared(),editedContact).enqueue(object:Callback<ContactResponse>{
           override fun onResponse(call: Call<ContactResponse>, response: Response<ContactResponse>) {
              if (response.isSuccessful && response.body()!=null){
                  successBlock.invoke()
              }else{
                  val data = Gson().fromJson(response.errorBody()!!.string(),ErrorResponse::class.java)
                  errorBlock.invoke(data.message)
              }
           }

           override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                t.message?.let { it -> errorBlock.invoke(it) }
           }

       })
    }

}