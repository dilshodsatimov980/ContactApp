package uz.gita.contactbyretrofit.domain

import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Query
import uz.gita.contactbyretrofit.data.remote.request.CreateAccountRequest
import uz.gita.contactbyretrofit.data.remote.request.CreateContactRequest
import uz.gita.contactbyretrofit.data.remote.request.EditContactRequest
import uz.gita.contactbyretrofit.data.remote.request.LoginRequest
import uz.gita.contactbyretrofit.data.remote.request.VerifyCodeRequest
import uz.gita.contactbyretrofit.data.remote.response.ContactResponse
import uz.gita.contactbyretrofit.data.remote.response.CreateAccountResponse
import uz.gita.contactbyretrofit.data.remote.response.ErrorResponse
import uz.gita.contactbyretrofit.data.remote.response.LoginResponse

interface AppRepository {
    fun createAccount(account:CreateAccountRequest):LiveData<Result<Response<ErrorResponse>>>
    fun verifySmsCode( data: VerifyCodeRequest): LiveData<Result<Response<CreateAccountResponse>>>
    fun getAllContacts():LiveData<Result<List<ContactResponse>>>
    fun addContact(  data: CreateContactRequest) :LiveData<Result<Unit>>
    fun deleteContact( id:Int):LiveData<Result<Unit>>

    fun enterByLogin(login: LoginRequest):LiveData<Result<LoginResponse>>
    fun setTokenToShared(token:String)
    fun getTokenFromShared():String

    fun editContact(editedContact:EditContactRequest,successBlock:()->Unit,errorBlock:(String)->Unit)
}